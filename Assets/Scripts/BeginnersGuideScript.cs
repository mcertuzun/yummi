﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeginnersGuideScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<CanvasGroup>().alpha == 1f)
        {
            gameObject.GetComponent<Animator>().SetBool("shining", true);
        }

        if (gameObject.GetComponent<CanvasGroup>().alpha == 0f)
        {
            gameObject.GetComponent<Animator>().SetBool("shining", false);
        }

        if (ScoreManager.score >= 2000)
        {
            gameObject.GetComponent<Animator>().enabled = false;
            gameObject.GetComponent<CanvasGroup>().alpha -= 0.01f;
        }

        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().simulated == false)
        {
            gameObject.GetComponent<Animator>().enabled = false;
            gameObject.GetComponent<CanvasGroup>().alpha = 0f;
        }
    }
}
