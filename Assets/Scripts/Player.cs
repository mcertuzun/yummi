﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//bunu neden yaptığımızı anlatmadı ve bilmiyorum, ileride sıkıntı yaratmasın diye yazdım, araştıracağım.
[RequireComponent(typeof(Rigidbody2D))]

public class Player : MonoBehaviour
{
    CameraFollow cf;
    Animator ninjaAnimator;
    Rigidbody2D rb;
    float movement = 0f;
    public float movementSpeed = 10f;
    float myTime;
    Vector3 playerV3;
    GameObject leftSide;
    GameObject rightSide;
    Text GameOverText;
    Text RestartText;
    Text CheckpointText;
    Text goldText;
    float speed = 100f;
    float step;
    Vector3 leftSidee;
    bool onRight = false;
    bool onLeft = false;
    public static bool Yes = false;
    bool checkpoint ;
    bool isDeath = false;
    bool decLight = false;
    bool beforeCheck = false;
    public bool respawned = false;
    Vector3 spawnPosition = new Vector3();
    public Button checkpointButton;

    [Header("Jump")]
    public bool isGrounded;
    //public float jumpForce;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask groundType;

    private Animator anim;
    // Start is called before the first frame update
    private void Awake()
    { 
        ninjaAnimator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        GameOverText = GameObject.Find("GameOverText").GetComponent<Text>();
        RestartText = GameObject.Find("RestartText").GetComponent<Text>();
        CheckpointText = GameObject.Find("CheckpointText").GetComponent<Text>();
        goldText = GameObject.Find("GoldText").GetComponent<Text>();
        cf = GameObject.Find("Main Camera").GetComponent<CameraFollow>();
        leftSide = GameObject.Find("LeftSide");
        rightSide = GameObject.Find("RightSide");
        checkpoint = false;

    }
   
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        //step = speed * Time.deltaTime;
     
        movement = Input.GetAxis("Horizontal") * movementSpeed;
        myTime = myTime + Time.deltaTime;


        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, groundType);


        if (isGrounded)
        {
            anim.SetTrigger("takeOf");
            anim.SetBool("isJumping", false);
            //  rb.velocity = Vector2.up * jumpForce;

        }
        else
        {
            anim.SetBool("isJumping", true);
        }


        if (Input.GetKey("right"))
        {
            rb.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (Input.GetKey("left"))
        {
            rb.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        foreach (Touch touch1 in Input.touches)
        {
            if (touch1.position.x > (Screen.width / 2) && touch1.position.y <= (Screen.height / 2.5f))
            {
                rb.transform.rotation = Quaternion.Euler(0, 0, 0);
                movement = movementSpeed;
            }

            if (touch1.position.x <= (Screen.width / 2) && touch1.position.y <= (Screen.height / 2.5f))
            {
                rb.transform.rotation = Quaternion.Euler(0, 180, 0);
                movement = movementSpeed * -1.0f;
            }
        }

        /*
        if (Input.GetButtonDown("RightButton"))
        {
            rb.transform.rotation = Quaternion.Euler(0, 0, 0);
            rb.AddForce(new Vector2(movement * myTime, 0));
        }

        if (Input.GetButtonDown("LeftButton"))
        {
            rb.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        */
        if (transform.position.y * 100f  > ScoreManager.score)
        {
            ScoreManager.score = transform.position.y * 100f ;
        }

        /*
        if ( isDeath == true)
        {
            respawnCharacter();
        }
        */

        if (decLight)
        {
            RenderSettings.ambientLight = new Color(RenderSettings.ambientLight.r - 0.01f, RenderSettings.ambientLight.g - 0.01f, RenderSettings.ambientLight.b - 0.01f, RenderSettings.ambientLight.a - 0.01f);

            if (!beforeCheck)
            {
                decLight = false;
            }
        }


    }

    private void FixedUpdate()
    {
        Vector2 velocity = rb.velocity;
        velocity.x = movement;
        rb.velocity = velocity;


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == ("platform"))
        {
            // ninjaAnimator.Play("ninjaMovementAnim");
           // ninjaAnimator.Play("ninjaFinalAnim");
            onRight = false;
            onLeft = false;
        }
        

        if (collision.gameObject.tag == "enemy")
        {
            cf.target = GameObject.Find("Background").transform;
            SpriteRenderer[] sprs = GameObject.FindGameObjectWithTag("Player").GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer spr in sprs)
            {
                spr.enabled = false;
            }

            decLight = true;

            weapon wea = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<weapon>();
            wea.enabled = false;
            GameObject.Find("CharacterLight").GetComponent<LightMechanics>().enabled = false;

            GameOverText.text = "Game Over";
            RestartText.text = "Press here to Restart";
            goldText.text = "Your Gold; " + (int)ScoreManager.gold;
            CheckpointText.text = "Spend 1000 gold to continue";

            if (respawned)
            {
                checkpointButton.gameObject.SetActive(false);
            }

            checkpointButton.GetComponent<Button>().enabled = true;
            GameObject.Find("PressToRestartButton").GetComponent<Button>().enabled = true;
        }

        if (collision.gameObject.name == "Background")
        {
            playerV3 = transform.position;
            isDeath = true;
        }

        if (ScoreManager.score <= 2000)
        {
            beforeCheck = true;
        }


        /*    if (collision.gameObject.name == "RightSide" && onLeft == false)
            {
                playerV3 = new Vector3(leftSide.transform.position.x, transform.position.y, transform.position.z);     
                transform.position = Vector3.MoveTowards(transform.position, playerV3, speed);
                onRight = true;
            }

            if (collision.gameObject.name == "LeftSide" && onRight == false)
            {
                playerV3 = new Vector3(rightSide.transform.position.x, transform.position.y, transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, playerV3, speed);
                onLeft = true;
            }*/



    }
    
    public void respawnCharacter()
    {
        if (ScoreManager.gold >= 1000 && checkpoint == false)
        {
                //ScoreManager.gold = ScoreManager.gold - 1000;
                PlayerPrefs.SetFloat("TotalGold", ScoreManager.gold - 1000f);

                checkpoint = true;
                GameObject.Find("Main Camera").GetComponent<CameraFollow>().target = GameObject.FindGameObjectWithTag("Player").transform;
                Yes = true;
                spawnPosition.x = playerV3.x;
                spawnPosition.y = playerV3.y + 1f;
                spawnPosition.z = playerV3.z;
                GameObject go1 = (GameObject)Instantiate(Resources.Load("Platform"), spawnPosition, Quaternion.identity);
                LevelGenerator.count++;

                Vector3 target = new Vector3(go1.transform.position.x, go1.transform.position.y + 2f, go1.transform.position.z);

                transform.position = Vector3.MoveTowards(transform.position, target, speed);

                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().simulated = true;
                for (int i = 0; i < 3; i++)
                {
                    GameObject.FindGameObjectWithTag("Player").transform.GetChild(0).GetChild(i).GetComponent<SpriteRenderer>().enabled = true;
                }
                isDeath = false;
                respawned = true;

                checkpointButton.GetComponent<Button>().enabled = false;
                GameObject.Find("PressToRestartButton").GetComponent<Button>().enabled = false;
                GameObject.Find("CharacterLight").GetComponent<LightMechanics>().enabled = true;

                //Eğer oyuncu arkaplan kararmadan önce ölürse respawn çektiğinde ışığı açmak için burası çalışıyor.
                if (beforeCheck)
                {
                    RenderSettings.ambientLight = Color.white;
                    beforeCheck = false;
                }

                if (DestroyObjects.beforeCheck)
                {
                    RenderSettings.ambientLight = Color.white;
                    DestroyObjects.beforeCheck = false;
                }

        }
        PlayerPrefs.SetFloat("TotalGold", 1000f);

    }

}
