﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakePlatformScript : MonoBehaviour
{
    bool checkEnter = false;
    public Animator anim;
    Rigidbody2D rb;
    Player player;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        rb = GameObject.Find("Yummi").GetComponent<Rigidbody2D>();
        player = rb.GetComponent<Player>();
        
    }

    void Update()
    {
    
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player" &&  player.isGrounded)
        {
           
            checkEnter = true;
            //Animasyon hallolduğu zaman buraya animasyon oynatımı eklenecek.
            anim.Play("FakePlatfor");
            // playerObject = collision.gameObject;
            Destroy(gameObject, anim.GetCurrentAnimatorStateInfo(0).length - 0.6f);

        }
    }
   
}
