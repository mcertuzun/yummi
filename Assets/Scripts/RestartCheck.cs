﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartCheck : MonoBehaviour
{
    public static bool checking = false;
    public static bool closeBeginner = false;
    public static bool first = true;

    private void OnEnable()
    {
        DontDestroyOnLoad(this);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Start()
    {
        if (PlayerPrefs.GetInt("restartCheck") == 0)
        {
            PlayerPrefs.SetInt("restartCheck", 1);
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (checking)
        {
            GameObject.Find("UIManager").GetComponent<UIManager>().StartGame();
            GameObject.Find("BeginnersGuideGroup").GetComponent<CanvasGroup>().alpha = 0;
            GameObject.Find("BeginnersGuideGroup").GetComponent<CanvasGroup>().interactable = false;
            GameObject.Find("BeginnersGuideGroup").GetComponent<CanvasGroup>().blocksRaycasts = false;
            checking = false;
            closeBeginner = true;
        }
    }

    public void OnSceneLoaded(Scene sc, LoadSceneMode lsm)
    {
        if (first)
        {
            first = false;
        }
        else
        {
            checking = true;
        }


    }
}
