﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weapon : MonoBehaviour
{
    public GameObject projectile, lightObject, backgroundObject;
    public Transform shotPoint;
    public float shootBetweenTime;
    private float shotTime;
    public int decreaseLightValue;
    CanvasGroup back;
    LightMechanics lightMec;
    public static bool shooted;

    void Start()
    {
        shooted = false;
        lightObject = GameObject.Find("CharacterLight");
       
        lightMec = lightObject.GetComponent<LightMechanics>();

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        transform.rotation = rotation;

        if (shooted == false)
        {
            if (Input.GetMouseButtonDown(0) && lightMec.checkLightObj() && !GameObject.Find("MenuBackground").GetComponent<SpriteRenderer>().enabled)
            {
                if (Input.mousePosition.y >= Screen.height / 2.5)
                {
                    if ( lightMec.lightObjectLight.spotAngle > 38)
                    {
                        Vector3 position = new Vector3(transform.position.x, transform.position.y, 84.7f);
                        lightMec.decreaseLightWithAttack(decreaseLightValue);
                        Instantiate(projectile, position, transform.rotation);
                        //shotTime = Time.time * shootBetweenTime;
                        shooted = true;
                    }
                }

            }
        }
        
    }
}
