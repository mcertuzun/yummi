﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScaler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var width = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        var height = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        //menuBackground.transform.localScale.Set(worldScreenWidth / width, worldScreenHeight / height, transform.localScale.z);

        Vector3 xWidth = gameObject.transform.localScale;
        xWidth.x = (worldScreenWidth / width) * 34.8f;
        //xWidth.x = worldScreenWidth;
        gameObject.transform.localScale = xWidth;
        //transform.localScale.x = worldScreenWidth / width;
        Vector3 yHeight = gameObject.transform.localScale;
        yHeight.y = (worldScreenHeight / height) * 33.9f;
        //yHeight.y = worldScreenHeight;
        gameObject.transform.localScale = yHeight;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
