﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    Text scoreText, highestScoreText;
    public static float score=0;
    public static float highestScore=0;
    public static float gold = 0;
    

    void Awake()
    {
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        score = 0;
        highestScoreText = GameObject.Find("HighestScoreText").GetComponent<Text>();
    }

    void Update()
    {
        try
        {
            highestScore = PlayerPrefs.GetFloat("HighestScore");
            gold = PlayerPrefs.GetFloat("TotalGold");
        }
        catch(System.Exception e)
        {
        }

        scoreText.text = "Score: " + (int) score;
        highestScoreText.text = "Highest Score: " + (int)highestScore;
      
        if (score > highestScore)
        {
            highestScore = score;
        }

        PlayerPrefs.SetFloat("HighestScore", highestScore);
        PlayerPrefs.SetFloat("TotalGold", gold);
    }

}
