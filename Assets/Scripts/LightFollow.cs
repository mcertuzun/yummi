﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFollow : MonoBehaviour
{
    GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("Yummi");
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name == "BackgroundLight")
        {
            target = GameObject.Find("Background");
            gameObject.transform.position = new Vector3(GameObject.Find("Yummi").transform.position.x, GameObject.Find("Yummi").transform.position.y, target.transform.position.z + -6.0f);
        }

        else
        {
            gameObject.transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z + -6.0f);
        }

        
    }
}
