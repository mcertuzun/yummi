﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightMechanics : MonoBehaviour
{
    GameObject lightObject;
    public Light lightObjectLight;
    float currentTime=0;
    int checkTime=1;
    public bool incLight = false, decLight = false;
    public float currentLight=0;
    float maxLight = 40.0f;
    bool firstRun = true;
    public SimpleHealthBar lightBar;
    bool lightReady = true;
   
    void Start()
    {
        lightObject = GameObject.Find("CharacterLight");
        lightObjectLight = lightObject.GetComponent<Light>();

        currentLight = lightObjectLight.spotAngle;
        lightObjectLight.spotAngle = 0.0f;
    }

    void FixedUpdate()
    {

        currentTime += Time.deltaTime;
        //startDecrease();

        if (incLight)
        {
            increaseLight();
        }
        if (decLight)
        {
            decreaseLight();
        }
        if (lightObjectLight.spotAngle < 78.0f)
        {
            lightBar.UpdateBar(lightObjectLight.spotAngle - 38.0f, maxLight);
        }

        if (ScoreManager.score >= 2000f)
        {
            if (lightObjectLight.spotAngle < 78 && lightReady)
            {
                RenderSettings.ambientLight = new Color(RenderSettings.ambientLight.r - 0.01f, RenderSettings.ambientLight.g - 0.01f, RenderSettings.ambientLight.b - 0.01f, RenderSettings.ambientLight.a - 0.01f);
                lightObjectLight.spotAngle = lightObjectLight.spotAngle + 0.8f;
            }
            else
            {
                GameObject.Find("CharacterLight").GetComponent<LightMechanics>().startDecrease();
                lightReady = false;
            }

        }

        //RenderSettings.ambientLight = new Color(RenderSettings.ambientLight.r - 0.01f, RenderSettings.ambientLight.g - 0.01f, RenderSettings.ambientLight.b - 0.01f, 1);
        /*
        if (RenderSettings.ambientLight.r < 170)
        {
            RenderSettings.ambientLight = new Color(RenderSettings.ambientLight.r + 0.01f, RenderSettings.ambientLight.g + 0.01f, RenderSettings.ambientLight.b + 0.01f, 1);
        }
        */
    }

    public void increaseLight()
    {
        if (ScoreManager.score >= 2000f)
        {
            if (firstRun)
            {
                currentLight = lightObjectLight.spotAngle;

                firstRun = false;
            }

            if (lightObjectLight.spotAngle < currentLight + 8.0f)
            {

                lightObjectLight.spotAngle = lightObjectLight.spotAngle + 4f;

                if (lightObjectLight.spotAngle > 79.0f)
                {
                    lightObjectLight.spotAngle = 78.0f;
                    incLight = false;
                    currentLight = lightObjectLight.spotAngle;
                    firstRun = true;
                    return;
                }
            }

            else
            {
                incLight = false;
                currentLight = lightObjectLight.spotAngle;
                firstRun = true;
            }
        }
    }

    public void decreaseLight()
    {
        if (lightObjectLight.spotAngle > 38.0f)
        {
            lightObjectLight.spotAngle = lightObjectLight.spotAngle - 0.075f;
        }
        else
        {
            decLight = false;
        }
    }
    public void decreaseLightWithAttack(int decreaseVal)
    {
        if (lightObjectLight.spotAngle > decreaseVal)
        {
            lightObjectLight.spotAngle = lightObjectLight.spotAngle - decreaseVal;
        }
    }
    private void checkLight()
    {
        if (currentTime > checkTime)
        {
            increaseLight();
            checkTime++;
        }
    }

    public void startIncrease()
    {
        incLight = true;
    }

    public void startDecrease()
    {
        decLight = true;
    }
    public bool checkLightObj()
    {
        if (lightObjectLight || !lightObjectLight)
        {
            return true;
        }
        return false;
    }

}
