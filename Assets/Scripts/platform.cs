﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platform : MonoBehaviour
{
    Vector3 spawnPosition = new Vector3();
    Vector3 rightSidee;
    float minY = 0.7f;
    float maxY = 1.2f;
    float levelWidthRight;
    float levelWidthLeft;
    GameObject heigth;
    GameObject rightSide;
    public float jumpForce = 8f;
    static float maxPosition = -2.68f;
    public GameObject platformPrefab;
    // Vector3 spawnPosition = new Vector3();
    bool spawnEnemy = true;
    GameObject topSide, lastPlatform, bottomSide;
    public float distance;

    private void Start()
    {
        bottomSide = GameObject.Find("toDestroy");
        topSide = GameObject.Find("TopSide");
        platformPrefab = GameObject.Find("Platform");
        heigth = GameObject.Find("toDestroy");
        rightSide = GameObject.Find("RightSide");
        rightSidee = new Vector3(rightSide.transform.position.x, transform.position.y, transform.position.z);
        levelWidthRight = rightSidee.x;
        levelWidthLeft = -levelWidthRight;
    }
    private void Update()
    {
        //calculateDistance();
        
        if (transform.position.y < heigth.transform.position.y)
        {
            CreatePlatform();
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
       if( collision.relativeVelocity.y <= 0)
        {
            Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                Vector2 velocity = rb.velocity;
                velocity.y = jumpForce;
                rb.velocity = velocity;
            }
        }
    }

    private void CreatePlatform()
    {
        lastPlatform = LevelGenerator.platforms.Last.Value;

        spawnPosition.y += Random.Range(lastPlatform.transform.position.y + minY, lastPlatform.transform.position.y + maxY);
        spawnPosition.x = Random.Range(levelWidthLeft + 0.5f, levelWidthRight - 0.5f);
        spawnPosition.z = 90.7f;
        GameObject go = (GameObject)Instantiate(Resources.Load("Platform"), spawnPosition, Quaternion.identity);
        LevelGenerator.count++;
        LevelGenerator.platforms.AddLast(go);
  
        bool check = false;

        if (LevelGenerator.count < 101)
        {
            check = LevelGenerator.l0();
        }
        if (LevelGenerator.count > 100 && LevelGenerator.count <= 200)
        {
            check = LevelGenerator.l1();
        } 
        if (LevelGenerator.count > 200 && LevelGenerator.count <= 300)
        {
            check = LevelGenerator.l2();
        }
        if (LevelGenerator.count > 300 && LevelGenerator.count <= 400)
        {
            check = LevelGenerator.l3();
        }
        if (LevelGenerator.count > 400 && LevelGenerator.count <= 450)
        {
            check = LevelGenerator.l4();
        }
    }

    //public void calculateDistance()
    //{
        
    //    distance = transform.position.y - bottomSide.transform.position.y;

   // }
}
