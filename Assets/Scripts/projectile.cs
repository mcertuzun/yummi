﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour
{
    public float speed;
    float lifeTime;
    public GameObject explosion;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        /*invoke ile istediğimiz metodu belli bir zaman aralığında çalıştırabiliyoruz.
         * Ayrıca 3. parametresiyle yeniden başlatma süresinide verebiliriz.*/
        Invoke("DestroyProjectile", 2);

        player = GameObject.FindGameObjectWithTag("Player");
        Physics2D.IgnoreCollision(player.GetComponent<BoxCollider2D>(), this.GetComponent<CircleCollider2D>());
    }

    // Update is called once per frame
    void Update()
    {
       // transform.position = Vector2.MoveTowards(transform.position, Vector2.up, 3);

        transform.Translate(Vector2.up * speed * Time.deltaTime);   
    }
    void DestroyProjectile()
    {
       
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
        weapon.shooted = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "RightSide" || collision.gameObject.name == "LeftSide" || collision.gameObject.tag == "enemy")
        {
            DestroyProjectile();
           
        }
       
    }

   


}
