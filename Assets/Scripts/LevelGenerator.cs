﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelGenerator : MonoBehaviour
{
    GameObject rightSide;
    public GameObject lightFish;
    public GameObject platformPrefab;
    public GameObject boostedPlatform;
    public GameObject fakePlatform;
    public GameObject moveablePlatform;
   
    int numberOfPlatforms = 200;
    float levelWidthRight;
    float levelWidthLeft;
    float minY = 1f;
    float maxY = 2f;
    float firstMinY = -4.4f;
    float firstMaxY = -2.9f;
    Vector3 spawnPosition = new Vector3();
    static Vector3 sa = new Vector3();
    Vector3 rightSidee;
    bool forLevel1 = true;
    bool forLevel2 = true;
    bool forLevel3 = true;
    GameObject[] platformArray;
    static GameObject randomPlatform;
    static int[] randomEnemy, randomBooster, randomFake, randomMoveable, randomLight;
    int randomEnemyCurrent, randomBoosterCurrent, randomFakeCurrent, randomMoveableCurrent;
    public static int count;
   

    public static LinkedList<GameObject> platforms = new LinkedList<GameObject>();

    void Start()
    {
        resetVariables();
        randomEnemy = new int[50];
        randomBooster = new int[50];
        randomFake = new int[50];
        randomMoveable = new int[50];
        randomLight = new int[50];
        rightSide = GameObject.Find("RightSide");
        boostedPlatform = GameObject.Find("BoosterPlatform");
        fakePlatform = GameObject.Find("FakePlatform");
        moveablePlatform = GameObject.Find("MoveablePlatform");
        lightFish = GameObject.Find("LightFish");
        rightSidee = new Vector3(rightSide.transform.position.x, transform.position.y, transform.position.z);
        levelWidthRight = rightSidee.x;
        levelWidthLeft = -levelWidthRight;
        l0Setup();

        for (int i = 0; i < 100; i++)
        {
            if ( i == 0)
            {
                spawnPosition.y += Random.Range(firstMinY, firstMaxY);
                spawnPosition.x = Random.Range(levelWidthLeft + 0.5f, levelWidthRight - 0.5f);
                spawnPosition.z = 90.7f;
                GameObject go = (GameObject)Instantiate(Resources.Load("Platform"), spawnPosition, Quaternion.identity);
                platforms.AddFirst(go);
                count++;
                continue;
            }

            spawnPosition.y += Random.Range(0.3f, 0.7f);
            spawnPosition.x = Random.Range(levelWidthLeft + 0.5f, levelWidthRight - 0.5f);
            spawnPosition.z = 90.7f;
            GameObject go1 = (GameObject)Instantiate(Resources.Load("Platform"), spawnPosition, Quaternion.identity);
            platforms.AddLast(go1);
            count++;

            bool check = false;

            if (count < 101)
            {
                check = l0();
            }

            if (count == 101)
            {
                check = l1();
            }

           // if (platforms.Count > 100 && platforms.Count <= 250)
           // {
           //     check = l1();
           // }

            if (count == 201)
            {
                check = l2();
            }

            if (count == 301)
            {
                check = l3();
            }

            if (count == 401)
            {
                check = l4();
            }

            if (!check)
            {

            }
        }
    }

    private void Update()
    {
        Debug.Log(count);
        if (ScoreManager.score >= 2000f)
        {
            RenderSettings.ambientLight = new Color(RenderSettings.ambientLight.r - 0.01f, RenderSettings.ambientLight.g - 0.01f, RenderSettings.ambientLight.b - 0.01f, 1);
            GameObject.Find("CharacterLight").GetComponent<LightMechanics>().startDecrease();
        }
      
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

        if (count > 100 && count <= 200)
        {
         
            l1Setup();
        }

        if (count > 200 && count <= 300)
        {
           
            l2Setup();
        }

        if (count > 300 && count <= 400)
        {
            l3Setup();
        }

        if (count == 450)
        {
            Debug.Log("sa level4");
            l4Setup();
        }

        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().simulated == false)
        {
            restartOnDeath();
        }
    }

    private void l0Setup()
    {
        for (int i=0; i < 3; i++)
        {
            randomEnemy[i] = (int) Random.Range(0.0f, 100.0f);
        }
        for (int i = 0; i < 2; i++)
        {
            randomBooster[i] = (int)Random.Range(0.0f, 100.0f);
        }
        for (int i = 0; i < 10; i++)
        {
            randomFake[i] = (int)Random.Range(0.0f, 100.0f);
        }
        for (int i = 0; i < 7; i++)
        {
            randomMoveable[i] = (int)Random.Range(0.0f, 100.0f);
        }
        for (int i = 0; i < 25; i++)
        {
            randomLight[i] = (int)Random.Range(0.0f, 100.0f);
        }
    }

    public static bool l0()
    {
        try{
            for (int i = 0; i < 0; i++)
            {
                if (randomEnemy[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y + 0.5f;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("LightFish"), sa, Quaternion.identity);
                    return false;
                }
            }
      
            for (int i = 0; i < 2; i++)
            {
                if (randomBooster[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("BoostedPlatform"), sa, Quaternion.identity);
                    Destroy(platforms.Last.Value);
                    platforms.AddLast(createdSpec);
                    return true;
                }
            }

            for (int i = 0; i < 10; i++)
            {
                if (randomFake[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("FakePlatform"), sa, Quaternion.identity);
                    Destroy(platforms.Last.Value);
                    platforms.AddLast(createdSpec);
                    return true;
                }
            }

            for (int i = 0; i < 7; i++)
            {
                if (randomMoveable[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("MoveablePlatform"), sa, Quaternion.identity);
                    Destroy(platforms.Last.Value);
                    platforms.AddLast(createdSpec);
                    return true;
                }
            }

            for (int i = 0; i < 25; i++)
            {
                if (randomLight[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y + 0.5f;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("light"), sa, Quaternion.identity);
                    return true;
                }
            }

        }
        catch (System.Exception e)
        {
        }

        return false;
    }

    private void l1Setup()
    {
        //Doodler sayısı
        for (int i = 0; i < 5; i++)
        {
            randomEnemy[i] = (int) Random.Range(100.0f, 200.0f);
        }
        //Booster Platform sayısı
        for (int i = 0; i < 4; i++)
        {
            randomBooster[i] = (int)Random.Range(100.0f, 200.0f);
        }
        //Fake Platform sayısı
        for (int i = 0; i < 9; i++)
        {
            randomFake[i] = (int)Random.Range(100.0f, 200.0f);
        }
        //Moveable Platform sayısı
        for (int i = 0; i < 10; i++)
        {
            randomMoveable[i] = (int)Random.Range(100.0f, 200.0f);
        }
        //Toplanacak ışık sayısı
        for (int i = 0; i < 25; i++)
        {
            Debug.Log("yarattım");
            randomLight[i] = (int)Random.Range(100.0f, 200.0f);
        }
        /*
        GameObject.Find("Main Camera").GetComponent<Animator>().SetBool("cameraLightOpened", true);
        GameObject.Find("CharacterLight").GetComponent<Animator>().SetBool("characterLightOpened", false);
        */

    }

    public static bool l1()
    {
        try
        {
            for (int i = 0; i < 0; i++)
            {
                if (randomEnemy[i] == count)
                {
                   
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y + 0.5f;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("LightFish"), sa, Quaternion.identity);
                    return false;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                if (randomBooster[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("BoostedPlatform"), sa, Quaternion.identity);
                    Destroy(platforms.Last.Value);
                    platforms.AddLast(createdSpec);
                    return true;
                }
            }

            for (int i = 0; i < 9; i++)
            {
                if (randomFake[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("FakePlatform"), sa, Quaternion.identity);
                    Destroy(platforms.Last.Value);
                    platforms.AddLast(createdSpec);
                    return true;
                }
            }

            for (int i = 0; i < 10; i++)
            {
                if (randomMoveable[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("MoveablePlatform"), sa, Quaternion.identity);
                    Destroy(platforms.Last.Value);
                    platforms.AddLast(createdSpec);
                    return true;
                }
            }

            for (int i = 0; i < 25; i++)
            {
                if (randomLight[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y + 0.5f;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("light"), sa, Quaternion.identity);
                    return true;
                }
            }
        }
        catch (System.Exception e)
        {
        }

        return false;
    }

    public void l2Setup()
    {
        for (int i = 0; i < 4; i++)
        {
            randomEnemy[i] = (int)Random.Range(200.0f, 300.0f);
        }
        
       
        for (int i = 0; i < 25; i++)
        {
            Debug.Log("yarattım");
            randomLight[i] = (int)Random.Range(200.0f, 300.0f);
        }
    }

    public static bool l2()
    {


        randomPlatform = platforms.Last.Value;
        sa.y = randomPlatform.transform.position.y;
        sa.x = randomPlatform.transform.position.x;
        sa.z = 90.7f;
        GameObject createdSpec = (GameObject)Instantiate(Resources.Load("FakePlatform"), sa, Quaternion.identity);
        Destroy(platforms.Last.Value);
        platforms.AddLast(createdSpec);

        for (int i = 0; i < 25; i++)
        {
            if (randomLight[i] == count)
            {
                randomPlatform = platforms.Last.Value;
                sa.y = randomPlatform.transform.position.y + 0.5f;
                sa.x = randomPlatform.transform.position.x;
                sa.z = 90.7f;
                GameObject fakeLight = (GameObject)Instantiate(Resources.Load("light"), sa, Quaternion.identity);
                return true;
            }
        }

        return true;

        

    }

    public void l3Setup()
    {
        for (int i = 0; i < 3; i++)
        {
            randomEnemy[i] = (int)Random.Range(300.0f, 400.0f);
        }
        for (int i = 0; i < 2; i++)
        {
            randomBooster[i] = (int)Random.Range(300.0f, 400.0f);
        }
        for (int i = 0; i < 2; i++)
        {
            randomFake[i] = (int)Random.Range(300.0f, 400.0f);
        }
        for (int i = 0; i < 5; i++)
        {
            randomMoveable[i] = (int)Random.Range(300.0f, 400.0f);
        }
        //Toplanacak ışık sayısı
        for (int i = 0; i < 20; i++)
        {
            randomLight[i] = (int)Random.Range(300.0f, 400.0f);
        }
        /*
        GameObject.Find("Main Camera").GetComponent<Animator>().SetBool("cameraLightOpened", false);
        GameObject.Find("CharacterLight").GetComponent<Animator>().SetBool("characterLightOpened", true);
        */
    }

    public static bool l3()
    {
        try
        {
            for (int i = 0; i < 3; i++)
            {
                if (randomEnemy[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y + 0.5f;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("LightFish"), sa, Quaternion.identity);
                    return false;
                }
            }

            for (int i = 0; i < 2; i++)
            {
                if (randomBooster[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("BoostedPlatform"), sa, Quaternion.identity);
                    Destroy(platforms.Last.Value);
                    platforms.AddLast(createdSpec);
                    return true;
                }
            }

            for (int i = 0; i < 2; i++)
            {
                if (randomFake[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("FakePlatform"), sa, Quaternion.identity);
                    Destroy(platforms.Last.Value);
                    platforms.AddLast(createdSpec);
                    return true;
                }
            }

            for (int i = 0; i < 7; i++)
            {
                if (randomMoveable[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("MoveablePlatform"), sa, Quaternion.identity);
                    Destroy(platforms.Last.Value);
                    platforms.AddLast(createdSpec);
                    return true;
                }
            }

            for (int i = 0; i < 10; i++)
            {
                if (randomLight[i] == count)
                {
                    randomPlatform = platforms.Last.Value;
                    sa.y = randomPlatform.transform.position.y + 0.5f;
                    sa.x = randomPlatform.transform.position.x;
                    sa.z = 90.7f;
                    GameObject createdSpec = (GameObject)Instantiate(Resources.Load("light"), sa, Quaternion.identity);
                    return true;
                }
            }
        }
        catch (System.Exception e)
        {
        }

        return false;
    }

    public void l4Setup()
    {

        //Toplanacak ışık sayısı
       
        /*
        GameObject.Find("Main Camera").GetComponent<Animator>().SetBool("cameraLightOpened", true);
        GameObject.Find("CharacterLight").GetComponent<Animator>().SetBool("characterLightOpened", false);
        */
    }

    public static bool l4()
    {
        randomPlatform = platforms.Last.Value;
        sa.y = randomPlatform.transform.position.y;
        sa.x = randomPlatform.transform.position.x;
        sa.z = 90.7f;
        GameObject createdSpec = (GameObject)Instantiate(Resources.Load("MoveablePlatform"), sa, Quaternion.identity);
        Destroy(platforms.Last.Value);
        platforms.AddLast(createdSpec);
        return true;


    }

    public void resetVariables()
    {
        randomEnemy = new int[50];
        randomBooster = new int[50];
        randomFake = new int[50];
        randomMoveable = new int[50];
        count = 0;
        randomPlatform = null;
        sa = new Vector3();
        platforms = new LinkedList<GameObject>();
    }

    public void restartOnDeath()
    {
        if (Input.touches.Length != 0)
        {
            //SceneManager.LoadScene(0);
        }

    }
    private void lightGenerator()
    {

    }
}



