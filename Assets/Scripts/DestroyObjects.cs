﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DestroyObjects : MonoBehaviour
{

    static bool restart = false;
    Text GameOverText, RestartText, goldText, CheckpointText;
    bool hasHealth = false;
    bool checkEnter = false;
    bool isDeath = false;
    GameObject player;
    Vector3 playerV3;
    Vector3 spawnPosition = new Vector3();
    public static SpriteRenderer[] sprs;
    float speed = 500f;
    weapon wea;
    public static bool decLight = false;
    public static bool beforeCheck = false;


    public void Start()
    {
        wea = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<weapon>();
        sprs = GameObject.FindGameObjectWithTag("Player").GetComponentsInChildren<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player");
        GameOverText = GameObject.Find("GameOverText").GetComponent<Text>();
        RestartText = GameObject.Find("RestartText").GetComponent<Text>();
        goldText = GameObject.Find("GoldText").GetComponent<Text>();
        CheckpointText = GameObject.Find("CheckpointText").GetComponent<Text>();

        GameObject.Find("CheckpointButton").GetComponent<Button>().enabled = false;
        GameObject.Find("PressToRestartButton").GetComponent<Button>().enabled = false;

        decLight = false;
        beforeCheck = false;
    }

    public void Update()
    {
        if (Player.Yes == true)
        {
            GameOverText.text = "";
            RestartText.text = "";
            goldText.text = "";
            CheckpointText.text = "";
            wea.enabled = true;
            foreach (SpriteRenderer spr in sprs)
            {
                spr.enabled = true;
                
            }
            Player.Yes = false;
        }

        if (decLight)
        {
            RenderSettings.ambientLight = new Color(RenderSettings.ambientLight.r - 0.01f, RenderSettings.ambientLight.g - 0.01f, RenderSettings.ambientLight.b - 0.01f, RenderSettings.ambientLight.a - 0.01f);

            if(!beforeCheck)
            {
                decLight = false;
            }
        }

        /*if (isDeath == true)
        {

            if (Input.GetKeyDown("y"))
            {

                spawnPosition.x = playerV3.x;
                spawnPosition.y = playerV3.y + 1f;
                spawnPosition.z = playerV3.z;
                GameObject go1 = (GameObject)Instantiate(Resources.Load("Platform"), spawnPosition, Quaternion.identity);
                LevelGenerator.count++;

                Vector3 target = new Vector3(go1.transform.position.x, go1.transform.position.y + 2f, go1.transform.position.z);

                playerV3 = Vector3.MoveTowards(playerV3, target, speed);

                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().simulated = true;
                for (int i = 0; i < 3; i++)
                {
                    GameObject.FindGameObjectWithTag("Player").transform.GetChild(0).GetChild(i).GetComponent<SpriteRenderer>().enabled = true;
                }

                foreach (SpriteRenderer spr in sprs)
                {
                    spr.enabled = true;
                }

                // GameObject.Find("Background").GetComponent<EdgeCollider2D>().enabled = true;
                //playerV3.x = spawnPosition.x;
                //playerV3.y = spawnPosition.y + 2f;
                // playerV3.z = spawnPosition.z;
                isDeath = false;
            }
        }*/

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {


            GameObject.Find("Main Camera").GetComponent<CameraFollow>().target = GameObject.Find("Background").transform;
            GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().simulated = false;
            for (int i = 0; i < 3; i++)
            {
                GameObject.FindGameObjectWithTag("Player").transform.GetChild(0).GetChild(i).GetComponent<SpriteRenderer>().enabled = false;
            }

            decLight = true;
            if (ScoreManager.score <= 2000)
            {
                Debug.Log("fwefwfew");
                beforeCheck = true;
            }

            foreach (SpriteRenderer spr in sprs)
            {
                spr.enabled = false;
            }
            //Ateş etme mekaniğini durdurmak için
          
            wea.enabled = false;
            //Light bar durdurmak için.
            GameObject.Find("CharacterLight").GetComponent<LightMechanics>().enabled = false;

            ScoreManager.gold += ScoreManager.score / 200.0f;
            GameOverText.text = "Game Over";
            RestartText.text = "Press here to Restart";
            goldText.text = "Your Gold; " + (int)ScoreManager.gold;
            CheckpointText.text = "Spend 1000 gold to continue";
            //GameObject.Find("Background").GetComponent<EdgeCollider2D>().enabled = false;

            GameObject.Find("CheckpointButton").GetComponent<Button>().enabled = true;
            GameObject.Find("PressToRestartButton").GetComponent<Button>().enabled = true;

            if (player.GetComponent<Player>().respawned)
            {
                GameObject.Find("CheckpointButton").SetActive(false);
            }

            GameObject.Find("CharacterLight").GetComponent<LightMechanics>().enabled = false;

            isDeath = true;
            //if (hasHealth == true)
            //{

            //}

            PlayerPrefs.SetFloat("TotalGold", ScoreManager.gold);


        }

        if (collision.gameObject.tag == "enemy")
        {
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "light")
        {
            Destroy(collision.gameObject);
        }
    }

}
