﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    GameObject menuBackground;
    GameObject background;
    public GameObject bg;
    GameObject platform;
    GameObject levelGenerator;
    GameObject player;
    GameObject UIPlayer;
    GameObject weapon;
    GameObject characterLight;

    CanvasGroup hudGroup;
    CanvasGroup mainMenuGroup;
    CanvasGroup settingsMenuGroup;
    CanvasGroup inGameSettingsMenuGroup;
    CanvasGroup pauseMenuGroup;
    CanvasGroup beginnersGuideGroup;

    Toggle soundToggle;
    Toggle menuSoundToggle;

    public bool shine = true;
    int shineCount = 0;

    void Start()
    {
        menuBackground = GameObject.Find("MenuBackground");
        background = GameObject.Find("Background");
        platform = GameObject.Find("Platform");
        levelGenerator = GameObject.Find("LevelGeneratorObject");
        player = GameObject.FindGameObjectWithTag("Player");
        UIPlayer = GameObject.Find("UIYummi");
        weapon = GameObject.Find("weaponaa_0");
        characterLight = GameObject.Find("CharacterLight");

        hudGroup = GameObject.Find("HUDGroup").GetComponent<CanvasGroup>();
        pauseMenuGroup = GameObject.Find("PauseMenuGroup").GetComponent<CanvasGroup>();
        beginnersGuideGroup = GameObject.Find("BeginnersGuideGroup").GetComponent<CanvasGroup>();

        mainMenuGroup = GameObject.Find("MainMenuGroup").GetComponent<CanvasGroup>();
        settingsMenuGroup = GameObject.Find("SettingsMenuGroup").GetComponent<CanvasGroup>();
        inGameSettingsMenuGroup = GameObject.Find("InGameSettingsMenuGroup").GetComponent<CanvasGroup>();

        soundToggle = GameObject.Find("SoundToggle").GetComponent<Toggle>();
        soundToggle.onValueChanged.AddListener((value) =>
        {
            soundToggleMethod(value);
        });

        menuSoundToggle = GameObject.Find("MenuSoundToggle").GetComponent<Toggle>();
        menuSoundToggle.onValueChanged.AddListener((value) =>
        {
            soundToggleMethod(value);
        });

        //Ana menüde ışık rengi açılıyor.
        //RenderSettings.ambientLight = new Color(255f, 255f, 255f);

        //Oyun içindeki Yummi yi ana menüde kapanıyor.
        SpriteRenderer[] sps = player.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer sp in sps)
        {
            sp.enabled = false;
        }
    }

    void Update()
    {
        if (beginnersGuideGroup.alpha != 0)
        {
            if (shine)
            {
                if (beginnersGuideGroup.alpha >= 1)
                {
                    Debug.Log("qdwqwdqwd");
                    shineBeginner(true);
                    shine = false;
                }

                if (beginnersGuideGroup.alpha <= 0.5f)
                {
                    shineBeginner(false);
                    shine = false;
                }
            }
        }

    }

    public void HideShowCanvasGroup(CanvasGroup cg, bool ch, int i)
    {
        cg.alpha = i;
        cg.interactable = ch;
        cg.blocksRaycasts = ch;
    }

    //Bu method play butonuna tıkladığımızda çağırılan method. Burada gerekli scriptler açılıyor ve UI ile ilgili mevzular gerçekleşiyor.
    public void StartGame()
    {
        //Baştaki platform açılıyor.
        platform.GetComponent<SpriteRenderer>().enabled = true;
        platform.GetComponent<EdgeCollider2D>().enabled = true;
        //CanvasGrouplar düzenleniyor.
        HideShowCanvasGroup(hudGroup, true, 1);
        HideShowCanvasGroup(mainMenuGroup, false, 0);
        HideShowCanvasGroup(beginnersGuideGroup, true, 1);

        //Oyun background açılıyor.
        bg.GetComponent<SpriteRenderer>().enabled = true;
        bg.GetComponent<EdgeCollider2D>().enabled = true;
        
        try
        {

        }
        catch(System.Exception e)
        {

        }

        //Menu background kapanıyor.
        menuBackground.GetComponent<SpriteRenderer>().enabled = false;
        menuBackground.GetComponent<EdgeCollider2D>().enabled = false;

        SpriteRenderer[] sps2 = menuBackground.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer sp in sps2)
        {
            sp.enabled = false;
        }

        levelGenerator.GetComponent<LevelGenerator>().enabled = true;
        player.GetComponent<Player>().enabled = true;
        player.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        weapon.GetComponent<weapon>().enabled = true;
        characterLight.GetComponent<LightMechanics>().enabled = true;
        //RenderSettings.ambientLight = new Color(0f, 0f, 0f);

        SpriteRenderer[] sps1 = UIPlayer.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer sp in sps1)
        {
            sp.enabled = false;
        }

        SpriteRenderer[] sps = player.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer sp in sps)
        {
            sp.enabled = true;
        }

    }

    public void showInGameSettingsMenu()
    {
        HideShowCanvasGroup(inGameSettingsMenuGroup, true, 1);
        HideShowCanvasGroup(pauseMenuGroup, false, 0);
    }

    public void showSettingsMenu()
    {
        HideShowCanvasGroup(mainMenuGroup, false, 0);
        HideShowCanvasGroup(settingsMenuGroup, true, 1);
        HideShowCanvasGroup(pauseMenuGroup, false, 0);
    }

    public void showMainMenu()
    {
        HideShowCanvasGroup(mainMenuGroup, true, 1);
        HideShowCanvasGroup(settingsMenuGroup, false, 0);
    }

    public void showPauseMenu()
    {
        HideShowCanvasGroup(pauseMenuGroup, true, 1);
        HideShowCanvasGroup(inGameSettingsMenuGroup, false, 0);
    }

    public void resumeGame()
    {
        HideShowCanvasGroup(hudGroup, true, 1);
        if (!RestartCheck.closeBeginner && ScoreManager.score < 2000)
        {
            HideShowCanvasGroup(beginnersGuideGroup, true, 1);
            beginnersGuideGroup.gameObject.GetComponent<Animator>().enabled = true;
        }
        HideShowCanvasGroup(pauseMenuGroup, false, 0);
        Time.timeScale = 1;
    }

    public void stopGame()
    {
        HideShowCanvasGroup(hudGroup, false, 0);
        beginnersGuideGroup.gameObject.GetComponent<Animator>().enabled = false;
        HideShowCanvasGroup(beginnersGuideGroup, false, 0);     
        HideShowCanvasGroup(pauseMenuGroup, true, 1);
        Time.timeScale = 0;
    }

    public void restartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void respawnCharacter()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().respawnCharacter();
    }

    public void returnToMainMenu()
    {
        GameObject[] gs = GameObject.FindGameObjectsWithTag("RestartCheck");
        foreach (GameObject gg in gs)
        {
            gg.SetActive(false);
        }
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
        PlayerPrefs.SetInt("restartCheck", 0);
    }

    public void exitGame()
    {
        Application.Quit();
    }

    public void soundToggleMethod(bool val)
    {
        if (val)
        {
            AudioListener.pause = true;
        }

        if (!val)
        {
            AudioListener.pause = false;
        }
    }

    public void shineBeginner(bool x)
    {

        if (beginnersGuideGroup.alpha >= 1f)
        {
            shine = true;
            return;
        }

        if (beginnersGuideGroup.alpha <= 0.5f)
        {
            shine = true;
            return;
        }

        if (x)
        {
            beginnersGuideGroup.alpha = beginnersGuideGroup.alpha - 0.1f;
            shineBeginner(x);
        }

        else if (!x)
        {
            beginnersGuideGroup.alpha = beginnersGuideGroup.alpha + 0.1f;
            shineBeginner(x);
        }
    }
}
