﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Yummi : MonoBehaviour
{
    public float speed;
    public float moveInput;
    public float jumpForce;
    private Rigidbody2D rb;
    private Animator anim;

    private bool isGrounded;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask groundType;
    // Start is called before the first frame update
    private void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        moveInput = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);         
    }
    private void Update()
    {
        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, groundType);
        if (isGrounded == true && Input.GetKeyDown(KeyCode.Space)){
            anim.SetTrigger("takeOf");
            rb.velocity = Vector2.up * jumpForce;           
        }
        if (isGrounded == true)
        {
            anim.SetBool("isJumping", false);
        }
        else
        {
            anim.SetBool("isJumping", true);
        }

        if (moveInput == 0)
        {
            anim.SetBool("isRunnig", false);
        }
        else if(moveInput >0 || moveInput < 0)
        {
            anim.SetBool("isRunnig", true);
        }
     
    }


}
