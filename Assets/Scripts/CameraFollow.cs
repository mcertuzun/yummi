﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform target;
    float smoothSpeed = .3f;
    //private Vector3 currentVelocity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //target = player , transform = kamera
        if ( target.position.y > transform.position.y)
        {

            Vector3 newPos = new Vector3(transform.position.x, target.position.y, transform.position.z);
            transform.position = newPos;
            //Lerp() ve smoothDamp hareketleri daha smooth yapmak için kullanılan bir method, araştırın internetten kolay bir şey
            //transform.position = Vector3.SmoothDamp(transform.position, newPos, ref currentVelocity, smoothSpeed* Time.deltaTime); 
        }
    }

    private void LateUpdate()
    {
        
    }
}
